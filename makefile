SRCDIR = $(dir $(realpath $(firstword $(MAKEFILE_LIST))))
PREFIX ?= /usr
DESTDIR ?= $(PREFIX)/bin
VERCMPROOT ?= $(PREFIX)/share/vercmp
GITBASE ?= $(shell realpath $(shell git config --get remote.origin.url) | rev | \
           cut -d/ -f2- | rev)

lix-os-vercmp:
	git clone $(GITBASE)/vercmp-lix-os-formats format

bare-vercmp:
	git clone $(GITBASE)/vercmp-bare-formats format

install: $(SRCDIR)/format
	ln -sf $(SRCDIR) $(VERCMPROOT)
	ln -sf $(VERCMPROOT)/vercmp.sh $(DESTDIR)/vercmp

uninstall:
	rm $(DESTDIR)/vercmp
	rm $(VERCMPROOT)
